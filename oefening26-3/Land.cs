﻿namespace oefening26_3
{
    internal class Land
    {
        private string _naam;
        private string _hoofdstad;

        public Land() { }
        public Land(string naam, string hoofdstad)
        {
            _naam = naam;
            _hoofdstad = hoofdstad;
        }
        public string Naam { get { return _naam; } set { _naam = value; } }
        public string Hoofdstad { get { return _hoofdstad; } set { _hoofdstad = value; } }

        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Land r = (Land)obj;
                    if (this.GetHashCode() == r.GetHashCode())
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {
            return this.GetHashCode + Naam + " " + Hoofdstad;
        }
    }
}
