﻿namespace oefening26_3
{
    class Monarchie : Land
    {
        private string _koning;

        public Monarchie(string naam, string hoofdstad, string koning) : base(naam, hoofdstad)
        {
            koning = koning;
        }
        public string Koning
        {
            get { return _koning; }
            set { _koning = value; }
        }
        public override string ToString()
        {
            return $"{this.GetHashCode} (Koning:{Koning})";
        }
    }
}
