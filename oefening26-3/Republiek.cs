﻿namespace oefening26_3
{
    class Republiek : Land
    {
        private string _president;

        public Republiek(string naam, string hoofdstad, string president) : base(naam, hoofdstad)
        {
            President = president;
        }
        public string President
        {
            get { return _president; }
            set { _president = value; }
        }
        public override string ToString()
        {
            return $"{this.GetHashCode} (Presdinet:{President})";
        }
    }
}
